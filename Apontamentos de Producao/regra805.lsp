@==============================================================================@
@ Processar entrada de produção                                                @
@ Data : 05/07/2016                                                            @
@==============================================================================@
se (fnProcedimento = "ApontamentoProducao")
{
  nMetodo++;
  definir alfa sChave;
  definir interno.com.gs.g1.co.int.suprimentos.EntApontamentoProducao wsApontamento;
  
  wsApontamento.ModoExecucao = 1;
  wsApontamento.Chave = sChave;
  wsApontamento.Operacao = TipoOperacao;
  wsApontamento.Executar();
  
  definir alfa sRetorno;
  definir numero nRetorno;
  nRetorno = wsApontamento.TipoRetorno;
  sRetorno = wsApontamento.MensagemRetorno;
  
  se (nRetorno > 1)
  {
    ValStr = "Erro: " + sRetorno;
  }
  senao
  {
    ValStr = "Ok";
  }                 
}