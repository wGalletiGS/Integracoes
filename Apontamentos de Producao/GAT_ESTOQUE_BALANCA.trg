CREATE OR REPLACE TRIGGER GAT_ESTOQUE_BALANCA
  AFTER INSERT OR UPDATE OR DELETE ON GA_SAF_BALANCA
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE
  PRAGMA AUTONOMOUS_TRANSACTION;
  ERR_NUM  NUMBER;
  ERR_MSG  VARCHAR2(2000);
  ERR_TIPO NUMBER(1); --0-Erro, 1-Alerta, 2-Informativo
  SCODINTEGRACAO VARCHAR(20);
  NUNIDPROD      NUMBER(3);
  NUNID          NUMBER(4);
  SGRP           VARCHAR2(1);
  SBAL           VARCHAR2(1);
  SBALPESAGEM    VARCHAR2(1);
  SDIVI1         VARCHAR2(50);
  NCLASSIFICA    NUMBER(1);
  NFEZCLASSIFICA NUMBER(10);
  SCHAVEWBS VARCHAR2(50);
  SUSERBAL  VARCHAR2(50);
  NMOVSEQ INT;
  NTEMCLA INT;
  NTEMINT INT;
BEGIN
  -- DEFINE ERRO COMO 0
  ERR_TIPO := 0;
  -- RECEBE A CHAVE DE INTEGRA��O
  IF :NEW.BAL_TIPO_OPERACAO = 1 THEN
    -- CASO DE APONTAMENTO DE PRODU��O GERAR MD5 DA BALAN�A
    SCHAVEWBS := DB_INTEGRACAO.MD5('BAL|' || :NEW.COD_BALANCA);
  ELSE
    -- OUTROS CASOS
    SCHAVEWBS := NVL(:NEW.BAL_CHAVE_WBS, '');
  END IF;
  -- VERIFICA SE ESTA INSERINDO
  IF INSERTING THEN
    -- RECEBE SE CLASSIFICA��O
    SELECT COUNT(*)
      INTO NTEMCLA
      FROM GA_SAF_DESCONTOS
     WHERE COD_BALANCA = :NEW.COD_BALANCA
       AND COD_SAFRA = :NEW.COD_SAFRA
       AND COD_EMPR = :NEW.COD_EMPR;
    -- VERIFICA SE CLASSIFICA��O = 0
    IF NTEMCLA = 0 THEN
      -- SAI DA TRIGGER
      RETURN;
    END IF;
  END IF;
  -- VERIFICA SE ESTA ATUALIZANDO
  IF UPDATING THEN
    -- RECEBE QNT DE INTEGRA��ES
    SELECT COUNT(*)
      INTO NTEMINT
      FROM DB_INTEGRACAO.GATEC_MOV_ESTOQUE EST
     WHERE EST.CHAVE_REF = SCHAVEWBS
       AND EST.STATUS = 'S';
    -- VERIFICA SE N�O H� INTEGRACAO
    IF NTEMINT > 0 THEN
      -- VERIFICA SE PESO ESTA SENDO ALTERADO
      IF :NEW.BAL_PESO_LIQUIDO = :OLD.BAL_PESO_LIQUIDO THEN
        -- SAI DA TRIGGER
        RETURN;
      END IF;
    END IF;
  END IF;
  -- VERIFICA SE ESTA INSERINDO OU ATUALIZADO OU DELETANDO E SE A OPERA��O � APONTAMENTO DE PRODU��O
  IF (INSERTING OR UPDATING OR DELETING) AND NVL(:NEW.BAL_TIPO_OPERACAO, :OLD.BAL_TIPO_OPERACAO) = 1 THEN
    -- VERIFICA PESOS E DEPOSITOS
    IF (NVL(:NEW.BAL_PESO_SUBTOT, 0) <> 0 OR NVL(:OLD.BAL_PESO_SUBTOT, 0) <> 0) AND
       (NVL(:NEW.COD_LOCAL_ORI, NVL(:OLD.COD_LOCAL_ORI, '0')) <> '0' OR NVL(:NEW.COD_LOCAL_DES, NVL(:OLD.COD_LOCAL_DES, '0')) <> '0')
    THEN
      -- VERIFICA SE ESTA INSERINDO OU ATUALIZANDO
      IF (INSERTING OR UPDATING) THEN
        -- RECEBE PARAMETROS DOS ITENS
        SELECT NVL(ITE_GRP, 0),
               NVL(ITE_BAL, 0),
               NVL(ITE_BAL_PESAGEM, 0),
               ID_UNIDADE
          INTO SGRP, SBAL, SBALPESAGEM, NUNID
          FROM GA_SAF_ITEM
         WHERE TRIM(COD_ITEM) = TRIM(:NEW.COD_ITEM);
        -- RECEBE DADOS DA FAZENDA
        BEGIN
          SELECT D3.COD_DIVI1
            INTO SDIVI1
            FROM GA_SAF_DIVI3 D3, GA_SAF_DIVI4 D4
           WHERE D3.ID_DIVI3 = D4.ID_DIVI3
             AND D4.ID_DIVI4 = :NEW.ID_DIVI4;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_TIPO := 1;
            RAISE_APPLICATION_ERROR(-20500, 'Fazenda n�o localizada!');
        END;
        -- RECEBE CONFIGURA��O DE CLASSIFICA��O
        SELECT CFP_CLASSIFICACAO
          INTO NCLASSIFICA
          FROM GA_SAF_CONFIG_PESAGEM
         WHERE COD_ITEM = :NEW.COD_ITEM
           AND CFP_TIPO_OPERACAO = :NEW.BAL_TIPO_OPERACAO;
        -- VERIFICA SE FEZ CLASSIFICA��O
        SELECT COUNT(*)
          INTO NFEZCLASSIFICA
          FROM GA_SAF_DESCONTOS
         WHERE COD_BALANCA = :NEW.COD_BALANCA
           AND COD_SAFRA = :NEW.COD_SAFRA
           AND COD_EMPR = :NEW.COD_EMPR;
        -- VERIFICA PARAMETROS DE CLASSIFICA��O OBRIGATORIA, DEPOSITOS E SE TEM CLASSIFICA��O
        IF ((SBAL = '1' AND SGRP = '0') OR (SBAL = '1' AND SGRP = '1' AND SBALPESAGEM = '1')) AND
           (NVL(:NEW.COD_LOCAL_ORI, '0') <> '0' OR NVL(:NEW.COD_LOCAL_DES, '0') <> '0') AND
           ((NCLASSIFICA = 1 AND NFEZCLASSIFICA > 0) OR NCLASSIFICA = 0)
        THEN
          -- RECEBE DADOS DO ITEM
          BEGIN
            SELECT COD_INTEGRACAO, ID_UNIDADE
              INTO SCODINTEGRACAO, NUNIDPROD
              FROM GA_SAF_ITEM
             WHERE COD_ITEM = :NEW.COD_ITEM;
          EXCEPTION
            WHEN OTHERS THEN
              ERR_TIPO := 1;
              RAISE_APPLICATION_ERROR(-20500,
                                      'AVISO: Erro ao Localizar o Codigo da integracao e/ou Unidade do Produto: ' ||
                                      :NEW.COD_ITEM || '.');
          END;
          -- REMOVE REGISTRO ANTERIOR PARA A CHAVE NA TABELA DE WS
          DELETE FROM GATEC_MOV_ESTOQUE_WEBSERVICE WHERE CHAVE_REF = SCHAVEWBS;
          -- CRIA BKP DE REGISTRO NA TABELA DE WS
          INSERT INTO GATEC_MOV_ESTOQUE_WEBSERVICE
            (COD_EMPR,
             COD_ITEM,
             COD_LOCAL,
             DT_CRIACAO,
             QTD_TRANSACAO,
             QTD_PRIMARIA,
             MOV_REFERENCIA,
             COD_SAFRA,
             COD_DIVI1,
             ID_DIVI4,
             CAM_PLACA,
             STATUS,
             OBS,
             MOV_MODULO,
             ERRO,
             DSC_TRANSACAO,
             MOV_CONTA,
             CTB_CODIGO,
             COD_CENTR_CUSTO,
             COD_CULTURA,
             ID_UNIDADE,
             PROJETO,
             COD_LOCAL_TRANS,
             COD_EMPR_TRANS,
             DAT_IMPORT,
             DAT_EXPORT,
             USER_BAL,
             COD_BALANCA,
             CHAVE_SAPIENS,
             MOV_SEQ,
             CHAVE_REF)
            SELECT COD_EMPR,
                   COD_ITEM,
                   COD_LOCAL,
                   DT_CRIACAO,
                   QTD_TRANSACAO,
                   QTD_PRIMARIA,
                   MOV_REFERENCIA,
                   COD_SAFRA,
                   COD_DIVI1,
                   ID_DIVI4,
                   CAM_PLACA,
                   STATUS,
                   OBS,
                   MOV_MODULO,
                   ERRO,
                   DSC_TRANSACAO,
                   MOV_CONTA,
                   CTB_CODIGO,
                   COD_CENTR_CUSTO,
                   COD_CULTURA,
                   ID_UNIDADE,
                   PROJETO,
                   COD_LOCAL_TRANS,
                   COD_EMPR_TRANS,
                   DAT_IMPORT,
                   DAT_EXPORT,
                   USER_BAL,
                   COD_BALANCA,
                   CHAVE_SAPIENS,
                   MOV_SEQ,
                   SCHAVEWBS AS CHAVE
              FROM GATEC_MOV_ESTOQUE
             WHERE COD_EMPR = :NEW.COD_EMPR
               AND TRIM(COD_ITEM) = :NEW.COD_ITEM
               AND TRIM(MOV_REFERENCIA) =
                   :NEW.COD_EMPR || '-' || :NEW.COD_SAFRA || '-' || :NEW.COD_BALANCA
               AND MOV_MODULO = 'BAL'
               AND STATUS = 'N';
          -- REMOVE REGISTRO DA TABELA DE MOVIMENTO DE ESTOQUE CASO N�O ESTEJA INTEGRADO
          DELETE FROM GATEC_MOV_ESTOQUE
           WHERE COD_EMPR = :NEW.COD_EMPR
             AND TRIM(COD_ITEM) = :NEW.COD_ITEM
             AND TRIM(MOV_REFERENCIA) =
                 :NEW.COD_EMPR || '-' || :NEW.COD_SAFRA || '-' || :NEW.COD_BALANCA
             AND MOV_MODULO = 'BAL'
             AND STATUS = 'N';
          -- RECEBE USUARIO LAN�ADOR
          BEGIN
            IF NVL(NVL(:NEW.ID_USUARIO_ALTER, :NEW.ID_USUARIO), 0) <> 0 THEN
              SELECT DSC_USUARIO
                INTO SUSERBAL
                FROM GAM_USUARIOS
               WHERE ID_USUARIO =
                     NVL(NVL(:NEW.ID_USUARIO_ALTER, :NEW.ID_USUARIO), 0);
            ELSE
              SUSERBAL := '';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              SUSERBAL := '';
          END;
          -- RECEBE SEQUENCIA DE MOVIMENTO
          SELECT GASQ_MOV_ESTOQUE.NEXTVAL INTO NMOVSEQ FROM DUAL;
          -- INSERE REGISTRO NA TABELA DE MOVIMENTO DE ESTOQUE
          INSERT INTO GATEC_MOV_ESTOQUE
            (COD_EMPR,
             COD_SAFRA,
             MOV_REFERENCIA,
             ID_DIVI4,
             COD_DIVI1,
             COD_ITEM,
             DT_CRIACAO,
             CAM_PLACA,
             QTD_TRANSACAO,
             QTD_PRIMARIA,
             COD_LOCAL,
             STATUS,
             OBS,
             DSC_TRANSACAO,
             MOV_CONTA,
             ID_UNIDADE,
             MOV_MODULO,
             COD_BALANCA,
             CHAVE_REF,
             USER_BAL,
             MOV_SEQ)
          VALUES
            (:NEW.COD_EMPR,
             :NEW.COD_SAFRA,
             :NEW.COD_EMPR || '-' || :NEW.COD_SAFRA || '-' ||
             :NEW.COD_BALANCA,
             :NEW.ID_DIVI4,
             SDIVI1,
             SCODINTEGRACAO,
             :NEW.BAL_DTHR_1,
             :NEW.BAL_PLACA,
             :NEW.BAL_PESO_LIQUIDO,
             :NEW.BAL_PESO_LIQUIDO,
             :NEW.COD_LOCAL_DES,
             'N',
             'Entr.Prod.GATEC',
             'Entr.Prod.GATEC',
             'PROD',
             NUNIDPROD,
             'BAL',
             :NEW.COD_BALANCA,
             SCHAVEWBS,
             SUSERBAL,
             NMOVSEQ);
        END IF;
      -- DELETANDO TICKET
      ELSIF DELETING THEN
        -- RECEBE FAZENDA
        BEGIN
          SELECT D3.COD_DIVI1
            INTO SDIVI1
            FROM GA_SAF_DIVI3 D3, GA_SAF_DIVI4 D4
           WHERE D3.ID_DIVI3 = D4.ID_DIVI3
             AND D4.ID_DIVI4 = :OLD.ID_DIVI4;
        EXCEPTION
          WHEN OTHERS THEN
            ERR_TIPO := 1;
            RAISE_APPLICATION_ERROR(-20500, 'Fazenda nao localizada!');
        END;
        -- REMOVE REGISTRO DA INTEGRA��O
        DELETE FROM GATEC_MOV_ESTOQUE
         WHERE COD_EMPR = :OLD.COD_EMPR
           AND TRIM(COD_ITEM) = :OLD.COD_ITEM
           AND TRIM(MOV_REFERENCIA) =
               :OLD.COD_EMPR || '-' || :OLD.COD_SAFRA || '-' ||
               :OLD.COD_BALANCA
           AND MOV_MODULO = 'BAL'
           AND STATUS = 'N';
      END IF;
    END IF;
  END IF;
  -- COMMITA AS INFORMA��ES
  COMMIT;
-- ERROS
EXCEPTION
  WHEN OTHERS THEN
    -- RECEBE CODIGO DO ERRO SQL
    ERR_NUM := SQLCODE;
    -- RECEBE MENSAGEM SQL
    ERR_MSG := SQLERRM;
    -- FOR�A ROLLBACK DAS INFORMA��ES
    ROLLBACK;
    -- INSERE REGISTRO DE LOG
    INSERT INTO GATEC_LOG_INTEGRACAO
      (MOTIVO, DESCRICAO, DEPARTAMENTO, MODULO, CHAVE, TIPO, OPERACAO)
    VALUES
      ('Trigger GAT_ESTOQUE_BALANCA.',
       ERR_NUM || ' - ' || ERR_MSG || ' - Empr: ' || :NEW.COD_EMPR || ' - REF: ' ||
       NVL(:NEW.COD_BALANCA, :OLD.COD_BALANCA),
       'AGRO',
       'BAL',
       NVL(:NEW.COD_EMPR, :OLD.COD_EMPR) || '-' ||
       NVL(:NEW.COD_SAFRA, :OLD.COD_SAFRA) || '-' ||
       NVL(:NEW.COD_BALANCA, :OLD.COD_BALANCA),
       ERR_TIPO,
       '1');
    -- COMITA AS INFORMA��ES
    COMMIT;
    -- DISPARA ERRO PARA SISTEMA
    RAISE_APPLICATION_ERROR(-20500, ERR_NUM || ' - ' || ERR_MSG);
    RAISE;
END;

-------------------------------------------------------------------
-- Vers�o...........: 1.00.002
-- Data compila��o..: 10/05/2016
-- Autor............: Evandro
-- Chamado..........: 72297
-- Coment�rios......: Altera��es para gravar chave do webservice
-------------------------------------------------------------------
/
