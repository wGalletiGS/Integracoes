CREATE OR REPLACE PROCEDURE GAP_WEBSERVICE_DELETA_ESTOQUE(P_CHAVE_WBS VARCHAR2,
                                                          P_EMPRESA   NUMBER,
                                                          P_ERRO      VARCHAR2) IS

  SERRO      VARCHAR2(1000);
  sVALIDAWEB VARCHAR2(1); --Validacao de uso da integracao do WebService para uso tambem da GAP

  CURSOR C_MOV_ESTOQUE IS
    SELECT NVL(COD_BALANCA, '') COD_BALANCA, NVL(MOV_REFERENCIA, '') MOV_REFERENCIA,  NVL(MOV_SEQ, 0) MOV_SEQ, NVL(COD_ITEM, '') COD_ITEM
      FROM GATEC_MOV_ESTOQUE
     WHERE CHAVE_REF = P_CHAVE_WBS;

  R_MOV_ESTOQUE C_MOV_ESTOQUE%ROWTYPE;

BEGIN

  SERRO := SUBSTR(P_ERRO, 1, 1000);

  SELECT NVL(MAX(CFG_BAIXA_WEB_SERV), '0')
    INTO sVALIDAWEB
    FROM GA_SAF_CONFIG_GERAL CFG
   WHERE CFG.COD_EMPR = P_EMPRESA;
  --caso nao tenha controle de integracao pelo WEB SERVICE nao ativa a GAP
  IF sVALIDAWEB = 0 THEN
    RETURN;
  END IF;

  OPEN C_MOV_ESTOQUE;

  DELETE GATEC_MOV_ESTOQUE WHERE CHAVE_REF = P_CHAVE_WBS;
  DELETE FROM GATEC_MOV_ESTOQUE_WEBSERVICE WHERE CHAVE_REF = P_CHAVE_WBS;*/

  LOOP
    FETCH C_MOV_ESTOQUE
      INTO R_MOV_ESTOQUE;
    EXIT WHEN C_MOV_ESTOQUE%NOTFOUND;

    UPDATE GATEC_ALG_LOG_INTEGRACAO
       SET DATA = SYSDATE(), NR_ERRO = NVL(NR_ERRO, 0) + 1
     WHERE DESCRICAO = SERRO
       AND OBJETO =
           SUBSTR('GAP_WEBSERVICE_DELETA_NF - COD_BALANCA[' || R_MOV_ESTOQUE.COD_BALANCA || ']'
           || ' MOV_REFERENCIA [' || R_MOV_ESTOQUE.MOV_REFERENCIA || ']'
           || ' MOV_SEQ[' || NVL(R_MOV_ESTOQUE.MOV_SEQ, 0) || ']'
           || ' COD_ITEM[' || R_MOV_ESTOQUE.COD_ITEM || ']', 1, 200)

        AND CHAVE = 'CHAVE-[' || P_CHAVE_WBS || ']';

    IF SQL%NOTFOUND THEN
      INSERT INTO GATEC_ALG_LOG_INTEGRACAO
        (MOTIVO,
         DESCRICAO,
         DEPARTAMENTO,
         DATA,
         OBJETO,
         CHAVE,
         NR_ERRO,
         COD_EMPR)
      VALUES
        (SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
         SERRO,
         'AGRO',
         SYSDATE(),
        SUBSTR('GAP_WEBSERVICE_DELETA_NF - COD_BALANCA[' || R_MOV_ESTOQUE.COD_BALANCA || ']'
           || ' MOV_REFERENCIA [' || R_MOV_ESTOQUE.MOV_REFERENCIA || ']'
           || ' MOV_SEQ[' || NVL(R_MOV_ESTOQUE.MOV_SEQ, 0) || ']'
           || ' COD_ITEM[' || R_MOV_ESTOQUE.COD_ITEM || ']', 1, 200),
         'CHAVE-[' || P_CHAVE_WBS || ']',
         1,
         P_EMPRESA);
    END IF;

  END LOOP;
  ------------------------------------------------------------------
  -- VERSAO...........: 1.00.001
  -- DATA COMPILACAO..: 11/05/2016
  -- AUTOR............: EVANDRO
  -- CHAMADO..........: 72297
  -- COMENTARIOS......: Criacao do Objeto.
  -------------------------------------------------------------------
END;
/
